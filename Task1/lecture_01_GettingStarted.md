lecture 01 - Getting Started
===
## 目的：
* 理解tensorflow中的基本概念：Tensor、Graph、Session、Operation、Variable等
* 理解tensorflow中的基本操作：Build the Graph、Launching the Graph、Fetch、Feed等 
* 了解如何使用tensorflow构建神经

## 作业：

* 配置tensorflow开发环境（不需要提交）
* 读书报告 


## overview：
TensorFlow 是一个编程系统, 使用图来表示计算任务. 图中的节点被称之为 op (operation 的缩写).  一个 op获得 0 个或多个 Tensor , 执行计算, 产生 0 个或多个 Tensor .
每个 Tensor 是一个类型化的多维数组. 例如, 可以将一小组图像集表示为一个四维浮点数数组, 这四个维度分别是 [batch, height, width, channels] .
一个 TensorFlow 图描述了计算的过程. 为了进行计算, 图必须在 会话 里被启动. 会话 将图的 op 分发到诸如 CPU 或 GPU 之类的 设备 上, 同时提供执行 op 的方法. 这些方法执行后, 将产生的 tensor 返回. 在 Python 语言中, 返回的 tensor 是 numpy ndarray 对象

## 基本概念：
* Tensor: Tensor 可以看作是一个 n 维的数组或列表. 一个 tensor 包含一个静态类型 rank, 和 一个 shape.
* Graph: 表示计算任务
* Session：用来执行图

* Operation：图中的节点被称之为op（operation的缩写）

* Variable: 变量维护图执行过程中的状态信息

## 基本操作：
* Build the Graph

创建各种op<br>
例如常op：matrix1 = tf.constant([[3., 3.]])<br>
矩阵乘法 matmul op：product = tf.matmul(matrix1, matrix2)<br>
* Launching the Graph

``` python
sess = tf.Session()  
result = sess.run(product)# 调用 sess 的 'run()' 方法来执行矩阵乘法 op
sess.close()# 任务完成, 关闭会话
```

* Fetch

为了取回操作的输出内容, 可以在使用 Session 对象的 run() 调用 执行图时, 传入一些 tensor, 这些 tensor 会帮助你取回结果.

* Feed

Feed可以临时替代图中的任意操作中的 tensor 可以对图中任何操作提交补丁, 直接插入一个 tensor.feed 使用一个 tensor 值临时替换一个操作的输出结果. 可以提供 feed 数据作为 run() 调用的参数. feed
只在调用它的方法内有效, 方法结束, feed 就会消失. 最常见的用例是将某些特殊的操作指定为 "feed" 操作,标记的方法是使用 tf.placeholder() 为这些操作创建占位符.
例如：
``` python
input1 = tf.placeholder(tf.types.float32)
input2 = tf.placeholder(tf.types.float32)
output = tf.mul(input1, input2)
with tf.Session() as sess:
print sess.run([output], feed_dict={input1:[7.], input2:[2.]})
```

